var stripImageUrl = function(val){
	var temp = val.split && val.split('=') || val;
	temp = temp[0] || temp;
	if(temp.indexOf('//') == 0)
	{
		temp = 'https:' + temp;
	}
	return temp;
};

module.exports = {
	url: 'https://play.google.com/store/apps/details?id=%s&hl=%s',
	urlSearch: 'https://play.google.com/store/search?q=%s&c=apps',
	mainSelector: '',
	emptyDocument: '#error-section',
	emptySearch: 'div.empty-search',
	selectors: [
		{
			selector: '.buy-button-container.apps.play-button',
			attr: 'data-docid',
			property: 'bundleId'
		},
		{
			selector: '.document-title[itemprop=name]',
			property: 'title'
		},
		{
			selector: '.document-title[itemprop=name]',
			property: 'titleLowerCase',
			postHook: function(val) {
				return val.toLowerCase();
			}
		},
		{
			selector: '.text-body[itemprop=description] > [jsname]',
			property: 'description',
			type: 'html'
		},
		{
			selector: '.thumbnails > img.screenshot',
			attr: 'src',
			property: 'screenshots',
			type: [String],
			postHook: stripImageUrl
		},
		{
			selector: '.document-subtitle.primary span[itemprop=name]',
			property: 'developer'
		},
		{
			selector: '.cover-image[itemprop=image]',
			attr: 'src',
			property: 'icon',
			postHook: stripImageUrl
		},
		{
			selector: 'meta[itemprop=price]',
			attr: 'content',
			property: 'price',
			default: 0,
			postHook: function(val){
				try
				{
					val = val.match(/\d[0-9.]*/);
					val = val && Number(val) || 0; // Magic number
				}
				catch(e)
				{
					console.log('Cant parse price');
					val = 7.77; // Default
				}

				return val;
			}
		},
		{
			selector: '.document-subtitle.category',
			property: 'category'
		},
		{
			selector: '.document-subtitle.category',
			property: 'categories',
			attr: 'href',
			type: [String],
			postHook: function(val){
				return val.replace('/store/apps/category/', '');
			}
		},
		{
			selector: '.document-subtitle.category',
			property: 'categoryId',
			attr: 'href',
			type: String,
			postHook: function(val){
				if(this.categoryId)
				{
					return null;
				}
				return val.replace('/store/apps/category/', '');
			}
		},
		{
			selector: 'meta[itemprop=ratingValue]',
			attr: 'content',
			property: 'rating',
			default: 0,
			type: Number,
		},
		{
			selector: '.meta-info .content[itemprop=datePublished]',
			property: 'published'
		},
		{
			selector: 'meta[itemprop=ratingCount]',
			attr: 'content',
			property: 'ratingCount',
			default: 0,
			postHook: function(val) {
				var result = Number(val);
				if(isNaN(result))
				{
					result = 0;
				}
				return result;
			}
		},
		{
			selector: '.meta-info .content[itemprop=operatingSystems]',
			property: 'requiredOS'
		},
		{
			selector: '.meta-info .content[itemprop=softwareVersion]',
			property: 'currentVersion'
		},
		{
			selector: '.meta-info .content[itemprop=contentRating]',
			property: 'contentRating'
		},
		{
			selector: 'html',
			attr: 'lang',
			property: 'lang',
		},
	],
	selectorsSearch: [
		{
			selector: '.card-content-link',
			property: 'href'
		},
		{
			selector: '.card-click-target',
			property: 'href'
		},
	],
	userAgents: [
		'Mozilla/5.0 (Windows; U; Windows NT 6.1; zh-HK) AppleWebKit/533.18.1 (KHTML, like Gecko) Version/5.0.2 Safari/533.18.5',
		'Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US) AppleWebKit/533.19.4 (KHTML, like Gecko) Version/5.0.2 Safari/533.18.5',
		'Mozilla/5.0 (Windows; U; Windows NT 6.0; tr-TR) AppleWebKit/533.18.1 (KHTML, like Gecko) Version/5.0.2 Safari/533.18.5',
		'Mozilla/5.0 (Windows; U; Windows NT 6.0; nb-NO) AppleWebKit/533.18.1 (KHTML, like Gecko) Version/5.0.2 Safari/533.18.5',
		'Mozilla/5.0 (Windows; U; Windows NT 6.0; fr-FR) AppleWebKit/533.18.1 (KHTML, like Gecko) Version/5.0.2 Safari/533.18.5',
		'Mozilla/5.0 (Windows; U; Windows NT 5.1; zh-TW) AppleWebKit/533.19.4 (KHTML, like Gecko) Version/5.0.2 Safari/533.18.5',
		'Mozilla/5.0 (Windows; U; Windows NT 5.1; ru-RU) AppleWebKit/533.18.1 (KHTML, like Gecko) Version/5.0.2 Safari/533.18.5',
		'Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_5_8; zh-cn) AppleWebKit/533.18.1 (KHTML, like Gecko) Version/5.0.2 Safari/533.18.5',
		'Mozilla/5.0 (iPod; U; CPU iPhone OS 4_3_3 like Mac OS X; ja-jp) AppleWebKit/533.17.9 (KHTML, like Gecko) Version/5.0.2 Mobile/8J2 Safari/6533.18.5',
		'Mozilla/5.0 (iPod; U; CPU iPhone OS 4_3_1 like Mac OS X; zh-cn) AppleWebKit/533.17.9 (KHTML, like Gecko) Version/5.0.2 Mobile/8G4 Safari/6533.18.5',
		'Mozilla/5.0 (iPod; U; CPU iPhone OS 4_2_1 like Mac OS X; he-il) AppleWebKit/533.17.9 (KHTML, like Gecko) Version/5.0.2 Mobile/8C148 Safari/6533.18.5',
		'Mozilla/5.0 (iPhone; U; ru; CPU iPhone OS 4_2_1 like Mac OS X; ru) AppleWebKit/533.17.9 (KHTML, like Gecko) Version/5.0.2 Mobile/8C148a Safari/6533.18.5',
		'Mozilla/5.0 (iPhone; U; ru; CPU iPhone OS 4_2_1 like Mac OS X; fr) AppleWebKit/533.17.9 (KHTML, like Gecko) Version/5.0.2 Mobile/8C148a Safari/6533.18.5',
		'Mozilla/5.0 (iPhone; U; fr; CPU iPhone OS 4_2_1 like Mac OS X; fr) AppleWebKit/533.17.9 (KHTML, like Gecko) Version/5.0.2 Mobile/8C148a Safari/6533.18.5',
		'Mozilla/5.0 (iPhone; U; CPU iPhone OS 4_3_1 like Mac OS X; zh-tw) AppleWebKit/533.17.9 (KHTML, like Gecko) Version/5.0.2 Mobile/8G4 Safari/6533.18.5',
		'Mozilla/5.0 (iPhone; U; CPU iPhone OS 4_3 like Mac OS X; pl-pl) AppleWebKit/533.17.9 (KHTML, like Gecko) Version/5.0.2 Mobile/8F190 Safari/6533.18.5',
		'Mozilla/5.0 (iPhone; U; CPU iPhone OS 4_3 like Mac OS X; fr-fr) AppleWebKit/533.17.9 (KHTML, like Gecko) Version/5.0.2 Mobile/8F190 Safari/6533.18.5',
		'Mozilla/5.0 (iPhone; U; CPU iPhone OS 4_3 like Mac OS X; en-gb) AppleWebKit/533.17.9 (KHTML, like Gecko) Version/5.0.2 Mobile/8F190 Safari/6533.18.5',
		'Mozilla/5.0 (iPhone; U; CPU iPhone OS 4_2_1 like Mac OS X; ru-ru) AppleWebKit/533.17.9 (KHTML, like Gecko) Version/5.0.2 Mobile/8C148 Safari/6533.18.5',
		'Mozilla/5.0 (iPhone; U; CPU iPhone OS 4_2_1 like Mac OS X; nb-no) AppleWebKit/533.17.9 (KHTML, like Gecko) Version/5.0.2 Mobile/8C148a Safari/6533.18.5',

		'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/36.0.1944.0 Safari/537.36',
		'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.1916.47 Safari/537.36',
		'Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/34.0.1847.116 Safari/537.36 Mozilla/5.0 (iPad; U; CPU OS 3_2 like Mac OS X; en-us) AppleWebKit/531.21.10 (KHTML, like Gecko) Version/4.0.4 Mobile/7B334b Safari/531.21.10',
		'Mozilla/5.0 (Windows NT 6.2; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/32.0.1667.0 Safari/537.36',
		'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/32.0.1664.3 Safari/537.36',
	],
};