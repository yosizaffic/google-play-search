var util = require('util');
var _request = require('request');
var parser = require('whacko');
var config = require('./config');
var moment = require('moment');

exports.fetch = function(playId, lang, callback) {
	if(!callback)
	{
		callback = lang;
		lang = 'en';
	}
	var url = util.format(config.url, playId, lang);
	exports.request(url, function(error, response, body) {
		if(error)
		{
			return callback(error);
		}
		exports.parse(playId, body, lang, callback);

	});
}


exports.request = function(url, callback) {
	_request(url, callback);
}

exports.parse = function(playId, body, lang, callback) {
	if(!callback)
	{
		callback = lang;
		lang = 'en';
	}
	var result = {};
	var mainSelector = config.mainSelector;
	var selectors = config.selectors;
	var $ = parser.load(body);

	var errorElem = $(mainSelector + ' ' + config.emptyDocument);

	if(errorElem.length > 0)
	{
		callback(3, null);
		return;
	}

	for(var i = 0; i < selectors.length; i++)
	{
		var selector = selectors[i];

		var el = mainSelector + ' ' + selector.selector;
		var matchElements = $(el);
		var type = selector.type || 'text';

		matchElements.each(function(index, elem){

			var val;
			var match = $(elem);

			if(selector.attr)
			{
				val = match.attr(selector.attr);
			}
			else
			{
				var elementTypeHandler = match[type];
				if(!elementTypeHandler)
				{
					elementTypeHandler = match["text"];
					console.log('BundleId: ' + playId + '. Property: ' + selector.property + '. Cant find handler for type: ' + type);
				}
				if(elementTypeHandler)
				{
					val = elementTypeHandler.bind(match)();
					if(val && val.trim)
					{
						val = val.trim();
					}
					else
					{
						console.log('BundleId: ' + playId + '. Cant found a method trim() for selector: ' + selector.property);
					}
				}
				else
				{
					console.log('BundleId: ' + playId + '. Property: ' + selector.property + ' is impossible to extract');
					callback(3, null);
				}
			}

			if(selector.type == 'currency')
			{
				val = getCurrencyValue(val);
			}

			if(selector.type == Number)
			{
				val = Number(val);
			}

			if(selector.postHook)
			{
				val = selector.postHook.call(result, val);
			}

			if(selector.type && selector.type.constructor == Array)
			{
				result[selector.property] = result[selector.property] || [];
				result[selector.property].push(val);
			}
			else
			{
				result[selector.property] = result[selector.property] || val;
			}
		})

		if(result[selector.property] === undefined && 'default' in selector)
		{
			result[selector.property] = selector.default;
		}

	}

	if (!result.bundleId)
	{
		callback(new Error('Cant parse app: ' + playId + ', Lang: ' + lang), null);
		return;
	}

	result.lang = result.lang || lang || 'en';
	result.marketUrl = util.format(config.url, result.bundleId, lang);
	result.androidUrl = 'market://details?id=' + result.bundleId

	result.additionalInfo = extractAddtitionalInfo($);
	result.reviews = getReviews($, result.lang);

	exports.parseSearch(body, function(err, bundles) {

		if(!err)
		{
			result.related = bundles;
		}

		callback(null, result);

	})

}

exports.fetchSearch = function(query, callback) {
	var nums = 1000
	var url = util.format(config.urlSearch, query);
	if(query.indexOf('://play.google.com') > 0)
	{
		url = query;
		nums = 100;
	}

	var userAgentsLength = config.userAgents.length;
	var userAgent = config.userAgents[parseInt(Math.random() * userAgentsLength)];

	_request.post(
	{
		"url": url,
		"form": {
			"start": 0,
			"num": nums,
			"xhr": 1,
			"ipf": 1,
			"numChildren": 0
		},
		"headers": {
			'User-Agent': userAgent,
		},
	}, function(error, response, body) {
		if(error)
		{
			return callback(error);
		}

		exports.parseSearch(body, callback);

	});
};

exports.parseSearch = function(body, callback) {

	var selectorsSearch = config.selectorsSearch;

	var result = [];
	var bundles = {};

	var $ = parser.load(body);

	// Check empty result
	var emptyDivMatch = $(config.emptySearch);
	if(emptyDivMatch.length > 0)
	{
		callback(1, result);
		return;
	}

	selectorsSearch.forEach(function(selector) {
		var el = selector.selector;

		var match = $(el);
		for(i = 0; i < match.length; i++)
		{
			bundles[match[i].attribs.href.replace("/store/apps/details?id=", "")] = true;
		}

	});

	for(var bundle in bundles)
	{
		result.push(bundle);
	}

	if(result.length > 0)
	{
		callback(null, result);
		return;
	}

	// Something gone wrong
	callback(2, body);

}

function extractAddtitionalInfo($) {
	var result = [];
	try
	{
		$('.meta-info').each(function(i, j) {
			var meta = {};
			var key = $(j).children('.content').attr('itemprop');
			if(key)
			{
				meta[key.trim()] = $(j).children('.content').text().trim();
				result.push(meta);
			}
		});
	}
	catch(ex)
	{
		console.log(ex.message);
	}
	return result;
}

function getReviews($, lang) {
	var result = [];
	try
	{
		$('.single-review').each(function(i, j) {
			var meta = {};
			var elem = $(j);

			meta.author = elem.find('.author-name').text().trim();
			meta.origialDate = elem.find('.review-date').text().trim();
			meta.rating = elem.find('.current-rating').attr('style').replace('width: ', '').replace('%;', '');
			meta.title = elem.find('.review-title').text().trim();
			meta.text = elem.find('.review-body').text().replace(' Full Review', '').trim();

			if (lang == 'ar')
			{
				// In case of arabic date we should parse it in Saudi Arabia format
				lang = 'ar-sa';
			}

			var momentDate = moment(meta.origialDate, 'LL', lang);

			if (momentDate.isValid())
			{
				meta.date = momentDate.toDate();
			}
			else
			{
				console.error('Review date parsing error:', meta.origialDate);
				meta.date = new Date();
			}

			result.push(meta);
		});

	}
	catch(ex)
	{
		console.log(ex.message);
	}
	return result;
}

