# google-play-search 

> Crawls Google Play store for app details

## dependency Usage

add to your package.js

```
"dependencies": {
    "google-play-search": "https://bitbucket.org/appmahal/google-play-services/get/tip.tar.gz"
}
```

## Example

```js
var googlePlaySearch = require('google-play-search');

googlePlaySearch.fetch('com.google.android.music', function(err, gameData) {
  console.log(gameData);
});

googlePlaySearch.parse(htmlBody , function(err, gameData) {
  console.log(gameData);
});

googlePlaySearch.fetchSearch('a', function(err, gameData) {
  console.log(gameData);
});

googlePlaySearch.parseSearch(htmlBody, function(err, gameData) {
  console.log(gameData);
});

```

## Run tests for fetch :

```
npm test
```

